const AWS = require('aws-sdk');
const dynamodb = new AWS.DynamoDB();

async function copyTableData(sourceTable, destinationTable) {
  const scanParams = {
    TableName: sourceTable
  };

  let items = [];

  do {
    const scanResult = await dynamodb.scan(scanParams).promise();
    items = items.concat(scanResult.Items);
    scanParams.ExclusiveStartKey = scanResult.LastEvaluatedKey;
    console.log('items', items);
  } while (scanParams.ExclusiveStartKey);

  for (const item of items) {
    const putParams = {
      TableName: destinationTable,
      Item: item
    };
    await dynamodb.putItem(putParams).promise();
  }

  console.log('Data copied successfully');
}

const args = process.argv;

if (args.length !== 4) {
  console.log('Invalid arguments passed.');
  process.exit(1);
}

const sourceTable = args[2];
const destinationTable = args[3];

console.log('Source table', sourceTable);
console.log('Destination table', destinationTable);

copyTableData(sourceTable, destinationTable);
